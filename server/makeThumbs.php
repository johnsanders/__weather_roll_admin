<?php
	$savePath = $_POST["savePath"];
	$imgPath = $_POST["imgPath"];
	$filenames = explode(",", $_POST["filenames"]);
	$height = $_POST["height"];
	$width = $height * 16 / 9;

	foreach($filenames as $filename) {
		$file = $imgPath . "/" . $filename;
		echo file_exists($file) . " " . is_dir($file) . " " . $file;
		if ( file_exists($file) && !is_dir($file) ) {
			$thumb = new Imagick($imgPath . "/" . $filename);
			$thumb->sampleImage($height * 16/9, $height);
			$thumb->writeImage( $savePath . "/" . $filename);
			$thumb->destroy();
		}
	}
?>

