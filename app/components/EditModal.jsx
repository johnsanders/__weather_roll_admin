import React from "react";
import Modal from "react-modal";
import PropTypes from "prop-types";

const EditModal = (props) => {
	return (
		<Modal
			isOpen={props.activeImage.hasOwnProperty("filename")}
			contentLabel=""
			onRequestClose={props.handleClose}
			style={{
				overlay:{
					zIndex:2000,
					backgroundColor:"rgba(0,0,0,0.4)"
				},
				content:{
					zIndex:2001,
					textAlign:"center"
				}
			}}
		>
			<div className="row">
				<div className="col-xs-8 col-xs-offset-2">
					<form className="form form-horizontal">
						<div className="form-group">
							<h1>Edit Image</h1>
						</div>
						<div className="form-group">
							<img style={{height:"200px"}} src={props.imagePath + props.activeImage.filename} />
						</div>
						<div className="form-group">
							<label htmlFor="name">
								Name
							</label>
							<input
								type="text"
								id={"name_" + props.network}
								className="form-control"
								value={props.activeImage["name_" + props.network]}
								onChange={props.handleChange}
						/>
						</div>
						<div className="form-group">
							<label htmlFor="location">
								Location
							</label>
							<input
								type="text"
								id={"location_" + props.network}
								className="form-control"
								value={props.activeImage["location_" + props.network]}
								onChange={props.handleChange}
						/>
						</div>
						<div className="form-group">
							<button className="btn btn-primary" onClick={props.handleSubmit}>
								Save
							</button>
						</div>
					</form>
				</div>
			</div>

		</Modal>
	);
};
EditModal.propTypes = {
	imagePath:PropTypes.string.isRequired,
	activeImage:PropTypes.object,
	handleChange:PropTypes.func.isRequired,
	handleClose:PropTypes.func.isRequired,
	handleSubmit:PropTypes.func.isRequired,
	network:PropTypes.string.isRequired
};
export default EditModal;
