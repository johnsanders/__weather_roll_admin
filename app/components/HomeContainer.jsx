import React from "react";
import autoBind from "react-autobind";
import update from "immutability-helper";
import Home from "./Home";
import EditModal from "./EditModal";
import Notification from "./Notification";
import PropTypes from "prop-types";
import * as dbHelpers from "./helpers/dbHelpers";
import * as fileHelpers from "./helpers/fileHelpers";

class HomeContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {images:[], activeImageId:null, loading:false, notificationText:"", notificationStyle:"success"};
		this.network = props.match.params.network;
		autoBind(this);
	}
	componentDidMount() {
		this.syncFromDb();
	}
	handleImportNew() {
		this.setState({loading:true});
		fileHelpers.getNewAndDeletedFiles(this.state.images)
			.then(response => {
				const dbPromise = dbHelpers.addAndRemoveImages(response.newFiles, response.deletedFiles);
				const thumbPromise = fileHelpers.makeAndDeleteThumbs(response.newFiles, response.deletedFiles);
				Promise.all([dbPromise, thumbPromise])
					.then(() => {
						this.syncFromDb();
						this.setState({loading:false});
						const notificationMessage = dbHelpers.createAddDeleteNotification(response.newFiles, response.deletedFiles);
						this.setNotification(notificationMessage, "success", 4000);
					}).catch(err => {
						console.error(err);
						this.setNotification("File import partially failed.", "danger", 4000);
					});
			}).catch(err => {
				console.error(err);
				this.setNotification("File import failed.", "danger", 4000);
			});
	}
	setNotification(text, style, timeout) {
		this.setState({notificationText:text, notificationStyle:style});
		this.clearNotification(timeout);
	}
	clearNotification(timeout) {
		setTimeout(() => {
			this.setState({notificationText:"", notificationStyle:"success"});
		}, timeout);
	}
	syncFromDb() {
		dbHelpers.fetchFromDb()
			.then(response => this.setState({images:response.data.cakeWrWeatherrolls}))
			.catch(err => console.error(err));
	}
	handleChange(e) {
		const newImages = update(this.state.images, {[this.activeImageIndex]:{[e.target.id]:{$set:e.target.value}}});
		this.setState({images:newImages});
	}
	handleOpenEdit(e) {
		const id = parseInt(e.target.id.replace("edit_", ""));
		this.setState({activeImageId:id});
		this.activeImageIndex = this.getImageIndex(this.getImageById(id));
	}
	handleCloseEdit() {
		this.setState({activeImageId:null});
		this.activeImageId = null;
	}
	handleEditSubmit(e) {
		e.preventDefault();
		dbHelpers.saveToDb(this.state.images[this.activeImageIndex])
			.then(() => this.setNotification("Image data saved", "success", 3000))
			.catch(() => this.setNotification("Oops.. Image data not saved."))
			.then(() => this.setState({activeImageId:null}));
	}
	handleActiveToggle(e) {
		const imageId = parseInt(e.target.id.replace("active_", ""));
		const image = this.getImageById(imageId);
		const index = this.getImageIndex(image);
		const newActiveVal = !image[this.getNetProp("active")];
		const newImages = update(this.state.images, {[index]:{[this.getNetProp("active")]:{$set:newActiveVal}}});
		this.setState({images:newImages}, () => {
			dbHelpers.saveToDb(this.getImageById(imageId));

		});
	}
	getNetProp(prop) {
		return prop + "_" + this.network;
	}
	getImageIndex(image) {
		return this.state.images.indexOf(image);
	}
	getImageById(id) {
		return this.state.images.find(image => image.id === id) || {};
	}
	render() {
		return (
			<div>
				<Home
					images={this.state.images}
					imagePath="/utilities/weather_roll/tmp/"
					notificationText={this.state.notificationText}
					handleOpenEdit={this.handleOpenEdit}
					handleActiveToggle={this.handleActiveToggle}
					handleImportNew={this.handleImportNew}
					network={this.network}
					loading={this.state.loading}
				/>
				<EditModal
					imagePath={"/utilities/weather_roll/getImage.php?img=WEATHER/VIEWER_PHOTOS/"}
					activeImage={this.getImageById(this.state.activeImageId)}
					handleChange={this.handleChange}
					handleClose={this.handleCloseEdit}
					handleSubmit={this.handleEditSubmit}
					network={this.network}
				/>
				<Notification message={this.state.notificationText} style={this.state.notificationStyle}/>
			</div>
		);
	}
}
HomeContainer.propTypes = {
	match:PropTypes.object.isRequired
};
export default HomeContainer;
