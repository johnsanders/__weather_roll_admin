import React from "react";
import Nav from "./nav/Nav";
import {Link} from "react-router-dom";
import cnniLogo from "../img/cnni.png";
import cnneLogo from "../img/cnne.png";

const NetworkChooser = () => (
	<div>
		<Nav />
		<div className="page-header" style={{marginTop:"80px"}}>
			<h1 className="text-center text-uppercase">WEATHER ROLL ADMIN</h1>
		</div>
		<div className="row">
			<div className="col-xs-12">
				<h3 className="text-center">Which network do you want to work on?</h3>
			</div>
		</div>
		<div className="row" style={{marginTop:"50px"}}>
			<div className="col-xs-6 text-center">
				<Link to="/cnni">
					<img src={cnniLogo} />
				</Link>
			</div>
			<div className="col-xs-6 text-center">
				<Link to="/cnne">
					<img src={cnneLogo} />
				</Link>
			</div>
		</div>
	</div>
);

export default NetworkChooser;
