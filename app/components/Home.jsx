import React from "react";
import Nav from "./nav/Nav";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";
import ImageRow from "./ImageRow";
const networkNames = {
	cnni:"CNN International",
	cnne:"CNN en Español"
};
const Home = (props) => (
	<div>
		<Nav />
		<div className="page-header" style={{marginTop:"80px"}}>
			<h1 className="text-center text-uppercase">WEATHER ROLL ADMIN</h1>
			<h3 className="text-center">{networkNames[props.network]}</h3>
		</div>

		<table className="table table-striped table-bordered table-hover">
		<colgroup>
			<col span="1" style={{width:"10%"}} />
			<col span="1" style={{width:"20%"}} />
			<col span="1" style={{width:"20%"}} />
			<col span="1" style={{width:"20%"}} />
			<col span="1" style={{width:"10%"}} />
			<col span="1" style={{width:"10%"}} />
		</colgroup>
			<thead>
				<tr>
					<th>Image</th>
					<th>Filename</th>
					<th>Name</th>
					<th>Location</th>
					<th className="text-center">Active</th>
					<th className="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				{
					props.images.map(image => (
						<ImageRow
							key={image.id}
							image={image}
							network={props.network}
							imagePath={props.imagePath}
							handleOpenEdit={props.handleOpenEdit}
							handleActiveToggle={props.handleActiveToggle}
						/>
					))
				}
			</tbody>
		</table>
		<div className="row">
			<div className="col-xs-2">
				<button
					className="btn btn-default"
					onClick={props.handleImportNew}
				>
					Check for new & deleted images
				</button>
			</div>
			<div className="col-xs-1">
				<Spinner
					fadeIn="none"
					name="double-bounce"
					style={{display:props.loading ? "" : "none"}}
				/>
			</div>
		</div>
	</div>
);
Home.propTypes = {
	notificationText:PropTypes.string.isRequired,
	images:PropTypes.array.isRequired,
	imagePath:PropTypes.string.isRequired,
	handleOpenEdit:PropTypes.func.isRequired,
	handleActiveToggle:PropTypes.func.isRequired,
	handleImportNew:PropTypes.func.isRequired,
	network:PropTypes.string.isRequired,
	loading:PropTypes.bool.isRequired
};
export default Home;
