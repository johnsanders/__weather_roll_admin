import React from "react";
import PropTypes from "prop-types";

const ImageRow = props => (
	<tr>
		<td className="text-center"><img height="50" src={props.imagePath + props.image.filename} /></td>
		<td>{props.image.filename}</td>
		<td>{props.image["name_" + props.network]}</td>
		<td>{props.image["location_" + props.network]}</td>
		<td className="text-center">
			<button
				className={"btn " + "btn-" + (props.image["active_" + props.network] ? "success" : "default")}
				id={"active_" + props.image.id}
				onClick={props.handleActiveToggle}
			>
				{props.image["active_" + props.network] ? "Active" : "Inactive"}
			</button>
		</td>
		<td className="text-center">
			<button
				id={"edit_" + props.image.id.toString()}
				className="btn btn-primary"
				onClick={props.handleOpenEdit}>Edit
			</button>
		</td>
	</tr>
);

ImageRow.propTypes = {
	image:PropTypes.shape({
		filename:PropTypes.string.isRequired,
		id:PropTypes.number.isRequired
	}),
	imagePath:PropTypes.string.isRequired,
	network:PropTypes.string.isRequired,
	handleActiveToggle:PropTypes.func.isRequired,
	handleOpenEdit:PropTypes.func.isRequired
};

export default ImageRow;
