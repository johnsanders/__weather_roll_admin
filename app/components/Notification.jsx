import React from "react";
import PropTypes from "prop-types";
import CSSTransition from "react-transition-group/CSSTransition";
import TransitionGroup from "react-transition-group/TransitionGroup";

const SlideTransition = ({children, ...props}) => (
	<CSSTransition {...props} timeout={300} classNames="slide">
		{children}
	</CSSTransition>
);
const Notification = (props) => (
	<TransitionGroup>
		{(
			props.message.length === 0 ? null :
			<SlideTransition>
					<div
						className={"alert alert-" + props.style}
						role="alert"
					>
						{props.message}
					</div>
			</SlideTransition>
		)}
	</TransitionGroup>
);
Notification.propTypes = {
	message:PropTypes.string.isRequired,
	style:PropTypes.string.isRequired
};
export default Notification;
