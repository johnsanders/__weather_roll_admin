import axios from "axios";

const imgPath = "/mnt/viz/new_arkiv/WEATHER/VIEWER_PHOTOS";
const thumbsPath = "/home/cnnitouch/www/utilities/weather_roll/tmp/";
const listUrl = "/utilities/weather_roll/listDirectory.php?path=" + imgPath;
const makeThumbsUrl = "/utilities/weather_roll/makeThumbs.php";
const deleteThumbsUrl = "/utilities/weather_roll/deleteThumbs.php";

const getServerFiles = () => {
	return new Promise((resolve, reject) => {
		axios.get(listUrl)
			.then(response => resolve(response.data))
			.catch(err => reject(err));
	});
};

const filterNewFiles = (serverFiles, dbFiles) => {
	return serverFiles.filter(serverFilename => (
		dbFiles.find(dbFile => dbFile.filename === serverFilename) === undefined
	));
};
const filterDeletedFiles = (serverFiles, dbFiles) => {
	return dbFiles.filter(dbFile => (
		serverFiles.find(serverFilename => dbFile.filename === serverFilename) === undefined
	));
};

export const getNewAndDeletedFiles = dbFiles => {
	return new Promise((resolve, reject) => {
		getServerFiles()
			.then(serverFiles => {
				const newFiles = filterNewFiles(serverFiles, dbFiles);
				const deletedFiles = filterDeletedFiles(serverFiles, dbFiles);
				resolve({newFiles, deletedFiles});
			}).catch(err => reject(err));
	});
};
export const makeAndDeleteThumbs = (toMake, toDelete) => {
	return new Promise((resolve, reject) => {
		const makePromise = makeThumbs(toMake);
		const deletePromise = deleteThumbs(toDelete);
		Promise.all([makePromise, deletePromise])
			.then(response => resolve(response))
			.catch(err => reject(err));
	});
};
const makeThumbs = filenames => {
	return new Promise((resolve, reject) => {
		const params = new URLSearchParams();
		params.append("filenames", filenames);
		params.append("savePath", thumbsPath);
		params.append("imgPath", imgPath);
		params.append("height", 50);
		axios.post(makeThumbsUrl, params)
			.then(response => {
				resolve(response.data);
			}).catch(err => reject(err));
	});
};

const deleteThumbs = images => {
	return new Promise((resolve, reject) => {
		const filenames = images.map(image => image.filename);
		const params = new URLSearchParams();
		params.append("filenames", filenames);
		params.append("path", thumbsPath);
		axios.post(deleteThumbsUrl, params)
			.then(response => {
				resolve(response.data);
			}).catch(err => reject(err));
	});
};
