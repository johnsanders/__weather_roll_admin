import axios from "axios";
const cakeBase = "/cake3/cnnitouch/cake-wr-weatherrolls/";
const imageBase = {
	name_cnni:"",
	name_cnne:"",
	location_cnni:"",
	location_cnne:"",
	active_cnni:false,
	active_cnne:false
};

export const saveToDb = (image) => {
	return axios.post(cakeBase + "edit/" + image.id + ".json", image);
};
export const fetchFromDb = () => {
	return axios.get(cakeBase + "index.json");
};
const addImages = filenames => {
	const images = filenames.map(filename => createImage(filename));
	return axios.post(cakeBase + "add.json", images);
};
const deleteImages = images => {
	const imageIds = images.map(image => image.id);
	return axios.post(cakeBase + "delete.json", imageIds);
};
export const addAndRemoveImages = (newFiles, deletedFiles) => {
	return new Promise((resolve, reject) => {
		const addPromise = addImages(newFiles);
		const deletePromise = deleteImages(deletedFiles);
		Promise.all([addPromise, deletePromise])
			.then(response => resolve(response))
			.catch(err => reject(err));
	});
};
const createImage = (filename) => {
	return Object.assign({}, imageBase, {filename:filename});
};

export const createAddDeleteNotification = (newFiles, deletedFiles) => {
	const newFileWord = newFiles.length === 1 ? " file " : " files ";
	const deletedFileWord = deletedFiles.length === 1 ? " file " : " files ";
	return newFiles.length.toString() + newFileWord + "imported. " +
		deletedFiles.length.toString() + deletedFileWord + "removed.";
};
