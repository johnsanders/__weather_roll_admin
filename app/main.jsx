import React from "react";
import {render} from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import "font-awesome/css/font-awesome.css";
import "./styles/style.css";
import HomeContainer from "./components/HomeContainer";
import NetworkChooser from "./components/NetworkChooser";

render((
	<BrowserRouter basename="/utilities/weather_roll">
		<Switch>
			<Route
				path="*/home"
				exact
				component={NetworkChooser}
			/>
			<Route
				path="*/:network"
				exact
				render={ (props) =>
					<HomeContainer {...props} />
				}
			/>
		</Switch>
	</BrowserRouter>
), document.getElementById("app"));
